/**
 * @(#) Waiter.java
 */

package restaurantGame;

import restaurantGame.utils.Experience;

public class Waiter extends Employee
{
	private int maxTables;
	private int noOfTables=0;
	
	@Override
	public void computeSalary() {
		Experience e = this.getExperience();
		switch(e){
		case LOW:
			this.salary=200;
			break;
		case AVERAGE:
			this.salary=300;
			break;
		case HIGH:
			this.salary=400;
			break;
		}
	}

	@Override
	protected void setTrainingCost() {
		this.trainingCost=800;
	}
	
	public void incrementNoOfTables(){
		this.noOfTables++;
	}
	public void setNoOfTables(int tables){
		this.noOfTables = tables;
	}
	public int getNoOfTables(){
		return this.noOfTables;
	}

	public int getMaxTables() {
		return maxTables;
	}

	public void setMaxTables(int maxTables) {
		this.maxTables = maxTables;
	}

}
