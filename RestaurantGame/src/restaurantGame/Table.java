/**
 * @(#) Table.java
 */

package restaurantGame;

public class Table
{
	private int number;
	
	//private Client client;
	private Waiter waiter;
	

	public Waiter getWaiter() {
		return waiter;
	}

	public void assignToWaiter(Waiter waiter )
	{
		this.waiter = waiter;
		waiter.incrementNoOfTables();
	}

	
}
