/**
 * @(#) MenuItem.java
 */

package restaurantGame;

import restaurantGame.utils.Quality;

public abstract class MenuItem{
	private double price;
	
	public MenuItem(String name, Quality qualityLevel, double price) {
		super();
		this.name = name;
		this.qualityLevel = qualityLevel;
		this.price = price;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName(){
		return this.name;
	}

	public void setQualityLevel(Quality qualityLevel) {
		this.qualityLevel = qualityLevel;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	private String name;
	
	protected Quality qualityLevel;
	
	public Quality getQualityLevel() {
		return qualityLevel;
	}
	
	public double getPrice() {
		return price;
	}
	
	public abstract double computeProductionPrice( );
	
	
}
