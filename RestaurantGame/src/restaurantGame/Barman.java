/**
 * @(#) Barman.java
 */

package restaurantGame;

import restaurantGame.utils.Experience;

public class Barman extends Employee
{

	@Override
	public void computeSalary() {
		Experience e = this.getExperience();
		switch(e){
		case LOW:
			this.salary=300;
			break;
		case AVERAGE:
			this.salary=400;
			break;
		case HIGH:
			this.salary=500;
			break;
		}
	}

	@Override
	protected void setTrainingCost() {
		this.trainingCost=1200;
		
	}
	
}
