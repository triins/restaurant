/**
 * @(#) Order.java
 */

package restaurantGame;

import java.util.Date;
import java.util.LinkedList;
import java.util.Random;

import restaurantGame.utils.Experience;
import restaurantGame.utils.Quality;

public class Order
{
	private Date time;
	private int orderNo;
	private int clientSatisfaction;
	private static int currentHighestOrderNumber = 0;
	
	private Client client;
	private Dish dish;
	private Beverage beverage;

	private Chef chef;
	private Barman barman;
	private Restaurant restaurant;
	
	public Order(Client c, Restaurant r){
		this.restaurant = r;
		this.client = c;
		this.dish = chooseRandomDish();
		this.beverage=chooseRandomBeverage();
		this.time = new Date();
		this.orderNo = getNewOrderNumber();
		//Find chef and barman
		LinkedList<Employee> employees = restaurant.getEmployees();
		for(Employee e : employees){
			if(e instanceof Chef){
				this.chef = (Chef) e;
			}else if(e instanceof Barman){
				this.barman = (Barman) e;
			}
		}
	}
	
	public Beverage getBeverage() {
		return beverage;
	}
	public Dish getDish(){
		return dish;
	}
	public int getClientSatisfaction(){
		calculateClientSatisfaction();
		return this.clientSatisfaction;
	}
	
	private void calculateClientSatisfaction(){
		int satisfaction = 0;
		if(clientSatisfiedWithService()){
			satisfaction ++;
		}else{
			satisfaction--;
		}
		if(clientSatisfiedWithDish()){
			satisfaction ++;
		}else{
			satisfaction--;
		}
		if(clientSatisfiedWithBeverage()){
			satisfaction ++;
		}else{
			satisfaction--;
		}
		
		/*
		//Client satisfaction cannot drop below 0 or raise above 100
		if(satisfaction<0){
			satisfaction = 0;
		}
		if(satisfaction>100){
			satisfaction=100;
		}*/
		System.out.println("Calculated client satisfaction points: " + satisfaction);
		this.clientSatisfaction=satisfaction;
		
	}
	
	private boolean clientSatisfiedWithService(){
		this.client.getTable();
		this.client.getTable().getWaiter();
		
		Experience experienceLevel = this.client.getTable().getWaiter().getExperience();
		int satisfactionPercentage=0;
		
		switch(experienceLevel){
		case HIGH:
			satisfactionPercentage = 90;
			break;
		case AVERAGE:
			satisfactionPercentage = 80;
		case LOW:
			satisfactionPercentage = 60;
			break;
		}
		
		Random rndGenerator = new Random();
		//Get a random number in the range 0..99
		int randomNumber = rndGenerator.nextInt(100);
		if(randomNumber<satisfactionPercentage){
			return true;
		}else{
			return false;
		}
	}
	
	private boolean clientSatisfiedWithDish(){
		Experience experienceLevel = this.chef.getExperience();
		int satisfactionPercentage=0;
		switch(experienceLevel){
		case HIGH:
			satisfactionPercentage = 80;
			break;
		case AVERAGE:
			satisfactionPercentage = 60;
		case LOW:
			satisfactionPercentage = 40;
			break;
		}
		
		if(this.dish.getQualityLevel()==Quality.HIGH){
			satisfactionPercentage +=20;
		}
		double priceMinusCost = this.dish.getPrice()-this.dish.computeProductionPrice();
		//For every 3� difference between price and cost, decrease satisfaction percentage by 10.
		int levels = (int)Math.floor(priceMinusCost/3);
		satisfactionPercentage -= levels*10;
		
		Random rndGenerator = new Random();
		//Get a random number in the range 0..99
		int randomNumber = rndGenerator.nextInt(100);
		if(randomNumber<satisfactionPercentage){
			return true;
		}else{
			return false;
		}
	}
	
	private boolean clientSatisfiedWithBeverage(){
		Experience experienceLevel = this.barman.getExperience();
		int satisfactionPercentage=0;
		
		switch(experienceLevel){
		case HIGH:
			satisfactionPercentage = 80;
			break;
		case AVERAGE:
			satisfactionPercentage = 60;
		case LOW:
			satisfactionPercentage = 40;
			break;
		}
		if(this.beverage.getQualityLevel()==Quality.HIGH){
			satisfactionPercentage +=20;
		}
		
		double priceMinusCost = this.dish.getPrice()-this.dish.computeProductionPrice();
		//For every 3� difference between price and cost, decrease satisfaction percentage by 10.
		int levels = (int)Math.floor(priceMinusCost/3);
		satisfactionPercentage -= levels*10;
		
		Random rndGenerator = new Random();
		//Get a random number in the range 0..99
		int randomNumber = rndGenerator.nextInt(100);
		if(randomNumber<satisfactionPercentage){
			return true;
		}else{
			return false;
		}
	}
	
	private int getNewOrderNumber(){
		currentHighestOrderNumber++;
		return currentHighestOrderNumber;
	}
	
	private Dish chooseRandomDish(){
		LinkedList<Dish> dishes = new LinkedList<Dish>();
		for(int i=0; i<restaurant.menu.length;i++){
			if(restaurant.menu[i] instanceof Dish){
				dishes.add((Dish)restaurant.menu[i]);
			}
		}
		Random rnd = new Random();
		int randomDishIndex = rnd.nextInt(dishes.size());
		return dishes.get(randomDishIndex);
	}
	
	private Beverage chooseRandomBeverage(){
		LinkedList<Beverage> beverages = new LinkedList<Beverage>();
		for(int i=0; i<restaurant.menu.length;i++){
			if(restaurant.menu[i] instanceof Beverage){
				beverages.add((Beverage)restaurant.menu[i]);
			}
		}
		Random rnd = new Random();
		int randomBeverageIndex = rnd.nextInt(beverages.size());
		return beverages.get(randomBeverageIndex);
	}
	
}
