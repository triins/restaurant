/**
 * @(#) Employee.java
 */

package restaurantGame;

import restaurantGame.utils.Experience;

public abstract class Employee
{
	private String name;
	
	private String surname;
	
	protected double salary;
	
	private Experience experience = Experience.LOW;
	
	protected double trainingCost;
	
	public Employee(){
		super();
		setTrainingCost();
	}
	
	protected abstract void setTrainingCost();
	
	public double getTrainingCost(){
		return this.trainingCost;
	}
	
	public Experience getExperience() {
		return experience;
	}

	public void setExperience(Experience experience) {
		this.experience = experience;
	}

	public abstract void computeSalary( );
	
	public void increaseExperience( )
	{
		switch(this.experience){
		case LOW:
			this.experience = Experience.AVERAGE;
			break;
		case AVERAGE:
			this.experience = Experience.HIGH;
			break;
		default:
			break;
		}
		System.out.println("Employee experience level has been raised to " + this.experience);
	}

	public double getSalary() {
		if(salary==0){
			computeSalary();
		}
		return salary;
	}
	
	public String toString(){
		return name + " " + surname;
	}
	
}
