/**
 * @(#) Player.java
 */

package restaurantGame;

public class Player
{
	private String name;
	
	private String score;
	
	private MenuItem menuItem;
	
	private Employee employee;
	
	private RankingList rankingList;
	
	public void setName( String name )
	{
		this.name = name;
	}
	
	public String getName( )
	{
		return this.name;
	}
	
}
