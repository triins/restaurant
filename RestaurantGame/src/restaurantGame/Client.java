/**
 * @(#) Client.java
 */

package restaurantGame;

import java.util.LinkedList;
import java.util.List;

public class Client
{
	private String name;
	
	private String surname;
	
	private String telephone;
	
	private String taxCode;
	
	private Table table;
	
	private Double moneySpent = 0.0; 
	
	private int averageBeverageVolume;
	
	private int averageCalories;
	
	private List<Dish> dishesOrdered = new LinkedList<Dish>();
	
	private List<Beverage> beveragesOrdered = new LinkedList<Beverage>();
	
	
	public Table getTable() {
		return table;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public void setMoneySpent(Double moneySpent) {
		this.moneySpent = moneySpent;
	}

	public void setAverageBeverageVolume(int averageBeverageVolume) {
		this.averageBeverageVolume = averageBeverageVolume;
	}

	public void setAverageCalories(int averageCalories) {
		this.averageCalories = averageCalories;
	}

	public void setDishesOrdered(List<Dish> dishesOrdered) {
		this.dishesOrdered = dishesOrdered;
	}

	public void setBeveragesOrdered(List<Beverage> beveragesOrdered) {
		this.beveragesOrdered = beveragesOrdered;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public void computeStatistics( )
	{
		int totalCalories = 0;
		int totalVolume = 0;
		System.out.println("Client: " + name + " " + surname);
		System.out.println("Dishes:");
	        for (Dish dish : dishesOrdered){
	        	System.out.println(dish.getName());
	        	//count same dishes
	        	//print out dishes and how many times they were ordered
	        	//System.out.println("%s: ordered %d times", dish, count);
	        	//count calories, etc
	        	totalCalories += dish.getCalorieCount();
	        	moneySpent += dish.getPrice();
	        }
	      	
	      	System.out.println("Beverages:");
	      	for (Beverage beverage : beveragesOrdered){
	      		System.out.println(beverage.getName());
	      		totalVolume += beverage.getVolume();
	      		moneySpent += beverage.getPrice();
	      	}
	      	this.averageCalories = totalCalories/dishesOrdered.size();
	      	this.averageBeverageVolume = totalVolume/beveragesOrdered.size();
	      	System.out.println("Average dish calorie count: " + averageCalories);
	      	System.out.println("Average beverage volume: " + averageBeverageVolume);
	      	System.out.println("Total money spent: " + moneySpent);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void addOrderedDish(Dish d){
		this.dishesOrdered.add(d);
	}
	public void addOrderedBeverage(Beverage b){
		this.beveragesOrdered.add(b);
	}
	
	/*
	public addOrderStatistics(Order o){
		
	}*/
}
