/**
 * @(#) GameController.java
 */

package restaurantGame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import restaurantGame.utils.Experience;
import restaurantGame.utils.Quality;


public class GameController
{
	private Restaurant restaurant;
	//there are 18 clients from whom to choose
	private Client clients[] = new Client[18];	
	private Player player;
	
	public String chooseName(){
		String name = "no-name-inserted";
		System.out.println("Please write your name");
		try{
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		    name = bufferRead.readLine();
		    if (name.length()!=0){
		    	System.out.println("Hello " + name + "!");	
		    }
		    else {
		    	System.out.println("The name must be inserted");
		    	chooseName();
		    }
		    
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return name;
	}
	
	public void startGame(){
		Player player = new Player();
		player.setName(chooseName());
		restaurant = new Restaurant();
		restaurant.setPlayer(player);
		
		prepareGame();
		initializeMenu();
		//weekly actions must be repeated 4 times to make a month
		int week = 0;
		while (week < 4){
			everyWeek();
			week++;
		}
		restaurant.payMonthlyAdditionalCosts();
		//Show statistics for clients: num each dish, bev, total money spent, average cal count and bev volume. client.computeStatistics();
		gameOver();
	}
	
	private void initializeMenu() {
		int highQualityDishes  = chooseNumberOfDishes();
		int highQualityBeverages = chooseNumberOfBeverages();
		double[] prices = choosePrices();

		MenuItem menu[] = new MenuItem[restaurant.getMenu().length];
		//create 5 dishes
		for (int i = 0; i<5; i++){
			if (highQualityDishes>0){
				highQualityDishes --;
				menu[i]= new Dish("Dish " + (i+1), Quality.HIGH, prices[0],
						300);
			}
			else{
				menu[i]= new Dish("Dish "+ (i+1), Quality.LOW, prices[1],
						500);
			}
		}
		for (int i = 5; i<10; i++){
			if (highQualityBeverages>0){
				highQualityBeverages--;
				menu[i]= new Beverage("Beverage " + (i-4), Quality.HIGH, prices[2],
						 500);
			}
			else{
				menu[i]= new Beverage("Beverage " + (i-4), Quality.LOW, prices[3],
						 300);
			}
		}
		restaurant.setMenu(menu);
		
				
	}
/*
	public void trainEmployee( Employee employee )
	{
		//Can't train an employee whose experience level is already high
		if(employee.getExperience()==Experience.HIGH){
			return;
		}
		
		restaurant.payTraining(employee.getTrainingCost());
		employee.increaseExperience();
	}
*/	
	
	public void matchTablesWithWaiters(List<Table> tables, List<Waiter> waiters){
		List<Table> resultTables = new LinkedList<Table>();
		Experience exp;
		Table table;
		//Try to give all tables to waiters with high experience
		for(Waiter f : waiters){
			exp = f.getExperience();
			if(exp==Experience.HIGH){
				//If experience level is high, try to give 3 tables
				for(int j=0;j<3&&tables.size()>0;j++){
					table = tables.get(0);
					resultTables.add(table);
					table.assignToWaiter(f);
					tables.remove(0);
				}
			}
		}
		
		//Give leftovers to waiters with average experience
		for(Waiter f : waiters){
			exp = f.getExperience();
			if(exp==Experience.AVERAGE){
				//If experience level is medium (rather than low), try to give 3 tables
				for(int j=0;j<3&&tables.size()>0;j++){
					table = tables.get(0);
					resultTables.add(table);
					table.assignToWaiter(f);
					tables.remove(0);
				}
			}
		}
		
		//Only low experience waiter left, give them the remaining tables
		for(Waiter f: waiters){
			for(int j=0;j<3&&tables.size()>0;j++){
				table = tables.get(0);
				resultTables.add(table);
				table.assignToWaiter(f);
				tables.remove(0);
			}
		}
		restaurant.setTables(resultTables);
		
		
	}
	
	private void everyWeek(){
		restaurant.orders = new LinkedList<Order>();
		//Set weekly ingredients cost to 0 in the beginning of every week
		restaurant.weeklyIngredientsCost = 0;
		//Double ingredientsCost = 0.0; 
		//repeat daily actions 7 times for a week
		int days = 0;
		while (days<7){
			everyDay();
			days++;
		}
		System.out.println("End of the week!");
		
		//End of every week
		for(Order o : restaurant.orders){
			o.getClientSatisfaction();
		}
		//Pay salaries	
		int salaryPaid = restaurant.paySalaries();
		System.out.println("Paid salaries: " + salaryPaid);
		//checks if player wants to train employees. Is possible only if enough money (>800)
				if (restaurant.getBudget()> 800){
					System.out.println("Do you want to train employees (Y/N)?");
					try{
					BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				    String trainEmployee = bufferRead.readLine();
				    if (trainEmployee.startsWith("Y") || trainEmployee.startsWith("y")){
				    	LinkedList<Employee> Employees = restaurant.getEmployees();
				    	LinkedList<Employee> trainedEmployees = new LinkedList<Employee>();
				    	for (Employee employee : Employees){
				    		if(employee.getExperience()!=Experience.HIGH){
				    			if(restaurant.getBudget()>employee.getTrainingCost()){
				    				System.out.println("Do you want to train " + employee.toString() + "(Y/N)?");
				    				BufferedReader bufferRead2 = new BufferedReader(new InputStreamReader(System.in));
				    			    String trainSpecificEmployee = bufferRead2.readLine();
				    			    if (trainSpecificEmployee.startsWith("Y") || trainSpecificEmployee.startsWith("y")){
				    			    	employee.increaseExperience();
				    			    	restaurant.payTraining(employee.getTrainingCost());
				    			    }
				    			}
				    		}
				    	trainedEmployees.add(employee);
				    	}
				    	restaurant.setEmployees(trainedEmployees);
				    }
					}
					catch(IOException e){
						e.printStackTrace();
					}
				}
		//Pay to suppliers
		restaurant.paySuppliers(restaurant.weeklyIngredientsCost);
		System.out.println("Paid ingredients: " + restaurant.weeklyIngredientsCost);
		//budget status and reputation points report
		System.out.println("Budget status: " + restaurant.getBudget());
		System.out.println("Reputation points: " + restaurant.getReputationPoints()); 
		
		
	}
	
	private void everyDay(){
		LinkedList<Waiter> waiters = new LinkedList<Waiter>();
		for(Employee e : restaurant.getEmployees()){
			if(e instanceof Waiter){
				waiters.add((Waiter) e);
			}
		}
		//Decides how many tables to assign to each waiter
		List<Table> tables = restaurant.populateTables(clients);		
		restaurant.setTables(tables);
		matchTablesWithWaiters(restaurant.getTables(), waiters);
		//call Order.calculateClientSatisfaction();
		if(restaurant.getBudget()<=0){
			gameOver();
		}
	}
	
	private void prepareGame(){
		Waiter waiter1 = new Waiter();
		Waiter waiter2 = new Waiter();
		Waiter waiter3 = new Waiter();
		Barman barman = new Barman();
		Chef chef = new Chef();
		LinkedList<Employee> employees = new LinkedList<Employee>();
		employees.add(waiter1);
		employees.add(waiter2);
		employees.add(waiter3);
		employees.add(barman);
		employees.add(chef);
		restaurant.setEmployees(employees);
		try{
		//"clients" contains data: name, surname, telephone, taxCode
			java.io.File clientsFile = new java.io.File("clients");
			java.util.Scanner sc = new java.util.Scanner(clientsFile);
			int clientOrderNr = 0;
		while (sc.hasNextLine()){
			Client client = new Client();
			String clientData = sc.nextLine();
			String[] clientDataUnits = clientData.split(", ");
			client.setName(clientDataUnits[0]);
			client.setSurname(clientDataUnits[1]);
			client.setTelephone(clientDataUnits[2]);
			client.setTaxCode(clientDataUnits[3]);
			clients[clientOrderNr] = client;
			clientOrderNr++;
		}
		sc.close();
		}
		catch(FileNotFoundException e){
		 e.printStackTrace();
		}
	}
	
	private int chooseNumberOfDishes(){
		int qualityH = 0;
		try{
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			
		    System.out.println("Insert the number of high quality dishes (1-5):");
		    String inQualityH = bufferRead.readLine();
		    
		    int qualityL = 0; 
		    if (inQualityH.length()!=0){
		    	qualityH = Integer.parseInt(inQualityH);
		    }
		    else {
		    	System.out.println("A number must be inserted");
		    	qualityH = chooseNumberOfDishes();
		    	return qualityH;
		    }
			System.out.println("Insert the number of low quality dishes (1-5):");
			String inQualityL = bufferRead.readLine();
			if (inQualityL.length()!=0){
				qualityL = Integer.parseInt(inQualityL);
			}
			else {
				System.out.println("A number must be inserted");
				qualityH = chooseNumberOfDishes();
				return qualityH;
			}
			if(qualityL+qualityH != 5){
				System.out.println("The sum of high and low quality dishes in menu has to be exactly 5!");
				qualityH = chooseNumberOfDishes();
				return qualityH;
			}			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return qualityH;
	}
	
	private int chooseNumberOfBeverages(){
		int BQualityH= 0; 
		try{
		//V�iks siin sama teha mis chooseNumberOfBeverages()
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Insert the number of high quality beverages (1-5):");
		BQualityH = Integer.parseInt(bufferRead.readLine());
		System.out.println("Insert the number of low quality beverages (1-5):");
		int BQualityL = Integer.parseInt(bufferRead.readLine());
		if(BQualityH+BQualityL != 5){
			System.out.println("The sum of high and low quality beverages in menu has to be exactly 5!");
			BQualityH=chooseNumberOfBeverages();
			return BQualityH;
		}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return BQualityH;
	}
	
	private double[] choosePrices(){
		double[] prices = new double[4];
		try{
		    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
		    System.out.println("Insert price for high quality dish");
		    prices[0] = Double.parseDouble(bufferRead.readLine());
		    System.out.println("Insert price for low quality dish");
		    prices[1] = Double.parseDouble(bufferRead.readLine());
		    System.out.println("Insert price for high quality beverage");
		    prices[2] = Double.parseDouble(bufferRead.readLine());
		    System.out.println("Insert price for low quality beverage");
		    prices[3] = Double.parseDouble(bufferRead.readLine());
		    
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return prices;
	}
	public void gameOver() { 
		System.out.println("Do you want to see customer statistics (Y/N)?");
		try{
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    String showStatistics = bufferRead.readLine();
	    if (showStatistics.startsWith("Y") || showStatistics.startsWith("y")){
			for (Client client : clients){
	    		client.computeStatistics();
	    	}
	    }
	    }
		catch(IOException e){
			e.printStackTrace();
		}
		System.out.println("Game over! Score: " + restaurant.getBudget());
	  }

}
