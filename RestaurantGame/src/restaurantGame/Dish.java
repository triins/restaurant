/**
 * @(#) Dish.java
 */

package restaurantGame;

import restaurantGame.utils.Quality;

public class Dish extends MenuItem{
	
	public Dish(String name, Quality qualityLevel, double price,
			int calorieCount) {
		super(name, qualityLevel, price);
		this.calorieCount = calorieCount;
	}

	private int calorieCount;
	
	public int getCalorieCount(){
		return calorieCount;
	}
	
	public double computeProductionPrice( )
	{
		if(this.qualityLevel==Quality.HIGH){
			return 10;
		}else {
			return 3;
		}
	}
	
}
