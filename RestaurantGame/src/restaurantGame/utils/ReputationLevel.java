package restaurantGame.utils;

public enum ReputationLevel {
	LOW, MEDIUM, HIGH
}
