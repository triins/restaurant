/**
 * @(#) Restaurant.java
 */

package restaurantGame;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import restaurantGame.utils.ReputationLevel;

public class Restaurant
{
	//since the name must not be inserted by player, will always be the same
	private String name = "Vegan kosher meat restaurant";
	
	private String address = "Beverly Hills 90210";
	
	private String city = "Los Angelos";
	
	private int budget=10000;
	
	private int reputationPoints=15;
	private ReputationLevel reputationLevel;
	
	private LinkedList<Employee> employees;
	
	private List<Table> tables;

	private Player player;

	public LinkedList<Order> orders = new LinkedList<Order>();
	public double weeklyIngredientsCost = 0;


	//Menu contains 5 dishes and 5 beverages, thus 10 items
	public MenuItem menu[] = new MenuItem[10];

	public MenuItem[] getMenu() {
		return menu;
	}

	public LinkedList<Employee> getEmployees() {
		return employees;
	}

	public void setMenu(MenuItem[] menu) {
		this.menu = menu;
	}

	public void setEmployees(LinkedList<Employee> employees) {
		this.employees = employees;
	}

	public Restaurant(){
		super();
		computeReputation();
	}
	
	public int getReputationPoints() {
		return reputationPoints;
	}

	public void setReputationPoints(int reputationPoints) {
		this.reputationPoints = reputationPoints;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	public void setPlayer(Player player){
		this.player = player;
	}
	

	public int getBudget(){
		return budget;
	}

	public void paySuppliers( Double amount )
	{
		budget -= amount;
	}
	
	public void computeReputation()
	{
		if(reputationPoints >= 30){
			this.reputationLevel = ReputationLevel.HIGH;
		}else{
			if(reputationPoints >= 15){
				this.reputationLevel = ReputationLevel.MEDIUM;
			}else{
				this.reputationLevel = ReputationLevel.LOW;
			}
		}
	}
	
	public void payUtilities( Double amount )
	{
		budget -= amount;
	}
	
	public int paySalaries()
	{
		int salaryToPay = 0;
		for(Employee e:employees){
			salaryToPay += e.getSalary();
		}
		budget -= salaryToPay;
		if(budget <= 0){
			System.out.println("Out of money! Game over!");
		}
		return salaryToPay;
	}
	
	public List<Table> populateTables(Client[] clients)
	{
		LinkedList<Table> tables = new LinkedList<Table>();
		int tablesToFill = numberOfTablesToFill();
		
		for(int i=0; i<tablesToFill;i++){
			Table table = new Table();
			
			Random rndGenerator = new Random();
			//there are 18 clients in given list
			int randomNumber1 = rndGenerator.nextInt(18);
			int randomNumber2 = rndGenerator.nextInt(18);		
			Client client1 = clients[randomNumber1];
			client1.setTable(table);
			Order o = new Order(client1, this);
			orders.add(o);
			client1.addOrderedBeverage(o.getBeverage());
			client1.addOrderedDish(o.getDish());
			addToWeeklyIngredientsCost(o.getBeverage().computeProductionPrice());
			addToWeeklyIngredientsCost(o.getDish().computeProductionPrice());
			receiveMoneyForOrder(o);
			
			Client client2 = clients[randomNumber2];
			client2.setTable(table);
			Order o2 = new Order(client2, this);
			orders.add(o2);
			client2.addOrderedBeverage(o2.getBeverage());
			client2.addOrderedDish(o2.getDish());
			addToWeeklyIngredientsCost(o2.getBeverage().computeProductionPrice());
			addToWeeklyIngredientsCost(o2.getDish().computeProductionPrice());
			receiveMoneyForOrder(o2);
			
			tables.add(table);
			
		}
		
		return tables;
	}
	
	public void payTraining(Double amount)
	{
		budget -= amount;
		System.out.println(amount + " paid for training.");
	}
	
	public void payMonthlyAdditionalCosts(){
		budget -= 4000;
	}
	
	
	private int numberOfTablesToFill(){
		switch(this.reputationLevel){
		case LOW:
			return 2;
		case MEDIUM:
			return 5;
		case HIGH:
			return 9;
		default:
			return 0;
		}
	}
	
	public void receiveMoneyForOrder(Order o){
		double amount = 0;
		amount += o.getBeverage().getPrice();
		amount += o.getDish().getPrice();
		budget += amount;
	}
	
	public void addToWeeklyIngredientsCost(double amount){
		this.weeklyIngredientsCost += amount;
	}
	
}
