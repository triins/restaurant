/**
 * @(#) Beverage.java
 */

package restaurantGame;

import restaurantGame.utils.Quality;

public class Beverage extends MenuItem{
	
	public Beverage(String name, Quality qualityLevel, double price,
			 int volume) {
		super(name, qualityLevel, price);
		this.volume = volume;
	}
	private int volume;	

	public double computeProductionPrice( )
	{
		if(this.qualityLevel==Quality.HIGH){
			return 3;
		}else {
			return 1;
		}
	}
	public int getVolume(){
		return this.volume;
	}
	
}
